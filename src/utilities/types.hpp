//
// Created by arnaldur on 11/13/17.
//

#ifndef ECLIPSE_TYPES_HPP
#define ECLIPSE_TYPES_HPP

#include <stdint-gcc.h>

typedef int_fast8_t  if8;
typedef int_fast16_t if16;
typedef int_fast32_t if32;
typedef int_fast64_t if64;

typedef uint_fast8_t  uf8;
typedef uint_fast16_t uf16;
typedef uint_fast32_t uf32;
typedef uint_fast64_t uf64;

#endif //ECLIPSE_TYPES_HPP
